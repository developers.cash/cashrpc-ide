/* eslint-env node */

/*
 * This file runs in a Node context (it's NOT transpiled by Babel), so use only
 * the ES6 features that are supported by your Node version. https://node.green/
 */

// Configuration for your app
// https://v2.quasar.dev/quasar-cli-vite/quasar-config-js

const { configure } = require('quasar/wrappers');
const nodePolyfills = require('vite-plugin-node-polyfills');
const topLevelAwait = require('vite-plugin-top-level-await').default;

// Default Application.
const DEFAULT_APP = 'CashRPC IDE';
const DEFAULT_DESCRIPTION = 'CashRPC IDE';
const DEFAULT_SITE = 'http://localhost:9000';

// Wallet Connect Defaults
const DEFAULT_WC_CHAIN = 'bch:bitcoincash';
const DEFAULT_WC_RELAY = 'wss://relay.walletconnect.com';
const DEFAULT_WC_PROJECT_ID = '5f1d2595f222a3b4999f9280c5d2e4ae';

const DEFAULT_ELECTRUM_SERVERS =
  'bch.imaginary.cash,cashnode.bch.ninja,electrum.imaginary.cash';

module.exports = configure(() => ({
  // TODO: Re-enable me.
  eslint: {
    // fix: true,
    // include = [],
    // exclude = [],
    // rawOptions = {},
    warnings: true,
    errors: true,
  },

  // app boot file (/src/boot)
  // --> boot files are part of "main.js"
  // https://v2.quasar.dev/quasar-cli-vite/boot-files
  boot: ['monaco', 'defaults', 'app'],

  // https://v2.quasar.dev/quasar-cli-vite/quasar-config-js#css
  // TODO: We can probably use an ENV var here to switch between P2P/Bull CSS.
  css: ['app.scss'],

  // https://github.com/quasarframework/quasar/tree/dev/extras
  extras: [
    'roboto-font', // optional, you are not bound to it
    // optional, you are not bound to it
    'material-icons',
  ],

  // Full list of options: https://v2.quasar.dev/quasar-cli-vite/quasar-config-js#build
  build: {
    target: {
      browser: ['es2022'],
      node: 'node20',
    },

    // Available values: 'hash', 'history'.
    vueRouterMode: 'hash',

    // Rebuilds Vite/linter/etc cache on startup
    rebuildCache: true,

    // Inject ENV variables into our code and set defaults.
    env: {
      // App-related.
      APP: process.env.APP || DEFAULT_APP,
      DESCRIPTION: process.env.DESCRIPTION || DEFAULT_DESCRIPTION,
      SITE: process.env.SITE || DEFAULT_SITE,

      // Electrum.
      ELECTRUM_SERVERS:
        process.env.ELECTRUM_SERVERS || DEFAULT_ELECTRUM_SERVERS,

      // Wallet Connect configurations.
      WC_CHAIN: process.env.WC_CHAIN || DEFAULT_WC_CHAIN,
      WC_RELAY: process.env.WC_RELAY || DEFAULT_WC_RELAY,
      WC_PROJECT_ID: process.env.WC_PROJECT_ID || DEFAULT_WC_PROJECT_ID,
    },

    // Extend the Vite Configuration to work-around CJS and GL Pages issues.
    extendViteConf(viteConf) {
      // NOTE: Quasar expects us to extend the Vite Configuration by re-assigning the viteConf parameter.
      /* eslint-disable no-param-reassign */

      // Allow relative URLs to work for Gitlab Pages deployment.
      viteConf.base = '';

      /*
      viteConf.build.minify = false;
      viteConf.optimizeDeps.disabled = false;
      */

      // viteConf.resolve.alias['antlr4ts'] = 'antlr4ts/bundle.js';

      viteConf.build.commonjsOptions = {
        // NOTE: We need this to compile AnyHedge Library + Dependencies to work with Vite.
        transformMixedEsModules: true,

        // dynamicRequireTargets: ['node_modules/antlr4ts/**/*.js'],
      };

      /* eslint-enable no-param-reassign */
    },

    // Instantiate Vite Plugins to work around Module issues.
    vitePlugins: [
      // Some of our libraries are not browser-friendly.
      // So, we have to declare these as externals.
      [
        nodePolyfills,
        {
          // To add only specific polyfills, add them here. If no option is passed, adds all polyfills
          include: ['path', 'util'],
          // To exclude specific polyfills, add them to this list. Note: if include is provided, this has no effect
          exclude: [
            'http', // Excludes the polyfill for `http` and `node:http`.
          ],
          // Whether to polyfill specific globals.
          globals: {
            Buffer: true, // can also be 'build', 'dev', or false
            global: true,
            process: true,
          },
          // Override the default polyfills for specific modules.
          overrides: {
            // Since `fs` is not supported in browsers, we can use the `memfs` package to polyfill it.
            fs: 'memfs',
          },
          // Whether to polyfill `node:` protocol imports.
          protocolImports: true,
        },
      ],
      // Some browsers do not support top-level-await (e.g. Falkon).
      // This is a polyfill for Vite that enables this support.
      [
        topLevelAwait,
        {
          // The export name of top-level await promise for each chunk module
          promiseExportName: '__tla',
          // The function to generate import names of top-level await promise in each chunk module
          promiseImportName: (i) => `__tla_${i}`,
        },
      ],
    ],
  },

  // Full list of options: https://v2.quasar.dev/quasar-cli-vite/quasar-config-js#devServer
  devServer: {
    // opens browser window automatically
    open: true,
    // Terminate if port is not available.
    strictPort: true,
    hmr: false,
  },

  // https://v2.quasar.dev/quasar-cli-vite/quasar-config-js#framework
  framework: {
    config: {
      // Default to light mode, and startup checks for client dark mode preference
      dark: false,
    },

    // Quasar plugins
    plugins: ['Dialog', 'Loading', 'Notify'],
  },

  // animations: 'all', // --- includes all animations
  // https://v2.quasar.dev/options/animations
  animations: ['fadeIn', 'pulse', 'tada'],

  // See: https://v2.quasar.dev/quasar-cli-vite/developing-pwa/configuring-pwa
  pwa: {
    workboxMode: 'generateSW',
    injectPwaMetaTags: true,
    swFilename: 'sw.js',
    manifestFilename: 'index.webmanifest',
    useCredentialsForManifestTag: false,
    // useFilenameHashes: true,
    // extendGenerateSWOptions (cfg) {}
    // extendInjectManifestOptions (cfg) {},
    // extendManifestJson (json) {}
    // extendPWACustomSWConf (esbuildConf) {}
  },
}));
