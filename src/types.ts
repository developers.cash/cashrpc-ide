import { Artifact } from 'cashscript';
import { AuthenticationTemplate } from '@bitauth/libauth';

// CashScript Contracts
export interface Contract {
  name: string;
  source: string;
  artifact?: Artifact;
  error?: string;
}

// Template Generation
export interface Template {
  options: string;
  template?: AuthenticationTemplate;
  error?: string;
}

// Tests belonging to signers
export interface Test {
  name: string;
  description: string;
  method: string;
  payload: string;
  response?: string;
  error?: string;
}

// Wallet Connect Signer
export interface Signer {
  chains: Array<string>;
  methods: Array<string>;
  events: Array<string>;
  template?: string;
  allowedTokens?: Array<string>;
}

// Project
export interface Project {
  version: string;
  name: string;
  description: string;
  contracts: Array<Contract>;
  template: Template;
  signer: Signer;
  tests: Array<Test>;
  signers: Array<Signer>;
}
