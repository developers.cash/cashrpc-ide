import WalletConnectReconnectDialogVue from '../components/WalletConnectReconnectDialog.vue';
import WalletConnectNewSessionDialogVue from '../components/WalletConnectNewSessionDialog.vue';

import {
  DialogCallback,
  BaseNewSessionDialog,
  BaseReconnectSessionDialog,
  BaseRequestDialog,
  CashConnectDapp,
  RequestOpts,
} from 'cashconnect';

import { computed, ref } from 'vue';
import { Dialog } from 'quasar';

import type { SignClientTypes } from '@walletconnect/types';

class NewSessionDialog extends BaseNewSessionDialog {
  dialog!: ReturnType<Dialog['create']>;

  constructor(url: string, cancelCallback: DialogCallback) {
    super(url, cancelCallback);

    this.dialog = Dialog.create({
      component: WalletConnectNewSessionDialogVue,
      componentProps: {
        url,
      },
    }).onCancel(() => {
      this.cancel();
      this.close();
    });
  }

  close() {
    try {
      this.dialog.hide();
    } catch (error) {}
  }
}

class ReconnectSessionDialog extends BaseReconnectSessionDialog {
  dialog!: ReturnType<Dialog['create']>;

  constructor(
    cancelCallback: DialogCallback,
    newSessionCallback: DialogCallback
  ) {
    super(cancelCallback, newSessionCallback);

    this.dialog = Dialog.create({
      component: WalletConnectReconnectDialogVue,
    })
      .onOk(() => {
        this.newSessionCallback();
        this.close();
      })
      .onCancel(() => {
        this.cancelCallback();
        this.close();
      });
  }

  close() {
    try {
      this.dialog.hide();
    } catch (error) {}
  }
}

class RequestDialog extends BaseRequestDialog {
  dialog!: ReturnType<Dialog['create']>;

  constructor(opts: RequestOpts, cancelCallback: DialogCallback) {
    super(opts, cancelCallback);

    // If this request should be visible...
    if (!opts.hide) {
      this.dialog = Dialog.create({
        message: opts.message || 'Approve in wallet',
        progress: true,
        ok: false,
        cancel: true,
      }).onCancel(() => {
        this.cancel();
        this.close();
      });
    }
  }

  close() {
    try {
      this.dialog.hide();
    } catch (error) {}
  }
}

export class CashConnectService extends CashConnectDapp {
  isPaired = ref(false);
  account = computed(() => this.session?.namespaces.bch?.accounts[0]);

  constructor(
    public readonly options: SignClientTypes.Options,
    public readonly chain: string = 'bitcoincash'
  ) {
    // Invoke the Dapp's constructor and use our WC Connect Modal.
    super(options, chain, {
      newSession: NewSessionDialog,
      reconnectSession: ReconnectSessionDialog,
      request: RequestDialog,
    });

    this.onConnected = async () => {
      this.isPaired.value = true;
    };

    this.on('disconnected', () => {
      this.isPaired.value = false;
    });
  }

  async pair(requiredNamespaces: any): Promise<boolean> {
    // If we are already authenticated, just return true.
    if (this.isPaired.value) {
      return true;
    }

    return await this.connect(requiredNamespaces);
  }
}
