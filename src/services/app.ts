import { ElectrumService } from './electrum';
import { CashConnectService } from './wallet-connect';
import type { Contract, Project } from 'src/types.js';

import { ref, toRaw, watch } from 'vue';
import type { Ref } from 'vue';
import { Notify, debounce, exportFile } from 'quasar';

import { compileString } from 'cashc-web';
import { get, set } from 'idb-keyval';
import { marked } from 'marked';

import ProjectDefault from '../templates/project_default.json';

export class App {
  // Services.
  electrum: ElectrumService;
  walletConnect: CashConnectService;

  // Help.
  help: Ref<{
    active: boolean;
    content: string;
  }> = ref({
    active: false,
    content: '',
  });

  // The active project
  project!: Ref<Project>;

  //---------------------------------------------------------------------------
  // Initialization
  //---------------------------------------------------------------------------

  constructor() {
    // Create the Electrum Service instance.
    // NOTE: List of servers is provided as an ENV var split by comma character.
    const electrumServers = (process.env.ELECTRUM_SERVERS as string).split(',');
    this.electrum = new ElectrumService(electrumServers);

    this.walletConnect = new CashConnectService(
      {
        relayUrl: process.env.WC_RELAY,
        projectId: process.env.WC_PROJECT_ID,
        metadata: {
          name: 'CashRPC IDE',
          description: 'CashRPC IDE',
          url: 'https://cashrpc-ide.developers.cash/',
          icons: ['https://cashrpc-ide.developers.cash/favicon.ico'],
        },
      },
      'bch:bchtest'
    );
  }

  async start(): Promise<void> {
    // Check that the user's browser is compatible.
    await this.checkBrowser();

    // Start the Electrum Service.
    await this.electrum.start();

    // Start the Wallet Connect Service.
    await this.walletConnect.start();

    // Load the default project (or a project from IndexedDB if it exists).
    await this.loadProject();
  }

  async checkBrowser(): Promise<void> {
    // Check that browser supports IndexedDB.
    try {
      // NOTE: It does not matter whether this key exists or not. If the browser does not support IndexedDB, this will throw an error.
      await get('isIndexedDbSupported');
    } catch (error) {
      throw new Error(
        'Your browser does not support IndexedDB. Please ensure you are not in a Private Window or switch to a Browser that supports IndexedDB'
      );
    }

    // Make sure that this browser supports Mutex Locks (navigator.lock).
    if (typeof navigator.locks === 'undefined') {
      throw new Error(
        'Your browser does not support Mutex Locks. Please update your browser or switch to a browser that supports this feature.'
      );
    }
  }

  async loadProject() {
    const existingProject = await get('project');

    if (existingProject) {
      this.project = ref(existingProject);
    } else {
      this.project = ref(ProjectDefault as Project);
    }

    if (!this.project.value.signer) {
      this.project.value.signer = {
        chains: ['bch:bchtest'],
        methods: ['bch_signTransaction_V0'],
        events: ['balancesChanged'],
        allowedTokens: ['*'],
        template: '',
      };
    }

    if (!this.project.value.tests) {
      this.project.value.tests = [];
    }

    // Setup a watcher to save the project to indexedDB.
    // NOTE: We debounce it so we're not constantly hammering the DB.
    watch(this.project, debounce(this.saveProject.bind(this), 5000), {
      deep: true,
    });
  }

  //---------------------------------------------------------------------------
  // Methods
  //---------------------------------------------------------------------------

  importProject(files: Array<{ name: string; content: string }>) {
    if (files.length !== 1) {
      throw new Error('Invalid file selected');
    }

    this.project.value = JSON.parse(files[0].content);
  }

  exportProject() {
    const filename = `${this.project.value.name}.json`;
    const contents = JSON.stringify(this.project.value, null, 2);

    const status = exportFile(filename, contents);

    if (status !== true) {
      Notify.create({
        message: 'Browser disallowed file download',
      });
    }
  }

  compileContract(contract: Contract) {
    try {
      // Clear state.
      contract.error = undefined;
      contract.artifact = undefined;

      contract.artifact = compileString(contract.source);
      contract.name = contract.artifact.contractName;
    } catch (error: any) {
      contract.error = error.message;
      throw error;
    }
  }

  compileContracts(): void {
    for (const contract of this.project.value.contracts) {
      this.compileContract(contract);
    }
  }

  async showHelp(content: string): Promise<void> {
    this.help.value.active = true;
    this.help.value.content = await marked.parse(content);
  }

  //---------------------------------------------------------------------------
  // Events/Callbacks
  //---------------------------------------------------------------------------

  async saveProject() {
    console.log('saving');
    await set('project', toRaw(this.project.value));
  }
}
