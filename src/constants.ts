export const WALLET_CONNECT_OPTIONS = {
  relayUrl: process.env.WC_RELAY,
  projectId: process.env.WC_PROJECT_ID,
  metadata: {
    name: process.env.NAME as string,
    description: process.env.DESCRIPTION as string,
    url: process.env.SITE as string,
    icons: [`${process.env.SITE as string}/favicon.ico`],
  },
};

export const WALLET_CONNECT_CHAIN = process.env.WC_CHAIN;
