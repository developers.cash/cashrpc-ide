export default {
  name: 'Sign Transaction',
  description: 'Example Sign Transaction',
  method: 'bch_signTransaction_V0',
  payload: JSON.stringify(
    [
      {
        userPrompt: 'Example Transaction',
        signerKey: 'user',
        transaction: {
          version: 2,
          locktime: 0,
          inputs: [],
          outputs: [
            {
              script: 'lock_example_sign',
              data: {
                example_data: '0xb33f',
              },
              valueSatoshis: 10_000,
            },
          ],
        },
      },
    ],
    null,
    2
  ),
};
