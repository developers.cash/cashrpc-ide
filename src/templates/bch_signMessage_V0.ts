export default {
  name: 'Sign Message',
  description: 'Example Sign Transaction',
  method: 'bch_signMessage_V0',
  payload: JSON.stringify(
    {
      // "Hello World!" as hex.
      message: '0x48656c6c6f20576f726c6421',
    },
    null,
    2
  ),
};
