import { boot } from 'quasar/wrappers';
import {
  Dark,
  QCard,
  QCardProps,
  QInput,
  QInputProps,
  QTable,
  QTableProps,
  QSelect,
  QSelectProps,
} from 'quasar';

/**
 * Set some default properties on a component
 */
/* eslint-disable @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unsafe-member-access, @typescript-eslint/no-explicit-any */
const SetComponentDefaults = <T>(
  component: any,
  defaults: Partial<T>
): void => {
  Object.keys(defaults).forEach((prop: string) => {
    component.props[prop] =
      Array.isArray(component.props[prop]) === true ||
      typeof component.props[prop] === 'function'
        ? {
            type: component.props[prop],
            default: (defaults as Record<string, any>)[prop],
          }
        : {
            ...component.props[prop],
            default: (defaults as Record<string, any>)[prop],
          };
  });
};
/* eslint-enable @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unsafe-member-access, @typescript-eslint/no-explicit-any */

export default boot(async () => {
  // Set Dark Mode.
  Dark.set(true);

  // QCard
  SetComponentDefaults<QCardProps>(QCard, {
    flat: true,
  });

  // QInput
  SetComponentDefaults<QInputProps>(QInput, {
    borderless: true,
  });

  // QSelect
  SetComponentDefaults<QSelectProps>(QSelect, {
    borderless: true,
  });

  // QTable
  SetComponentDefaults<QTableProps>(QTable, {
    flat: true,
  });
});
