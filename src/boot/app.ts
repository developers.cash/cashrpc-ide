import { App } from '../services/app';

import { boot } from 'quasar/wrappers';
import { Loading } from 'quasar';

import { defineCustomElements as QrCodeComponent } from '@bitjson/qr-code';

// Create a singleton of our app service.
export const app = new App();

// Boot (initialize) the service:
// For more info on params: https://v2.quasar.dev/quasar-cli/boot-files
export default boot(async () => {
  // Show the Loading Indicator.
  Loading.show();

  // Register BitJSON's QR Code WebComponent.
  QrCodeComponent(window);

  // Start the App service.
  await app.start();

  // Hide the loading indicator.
  Loading.hide();
});
