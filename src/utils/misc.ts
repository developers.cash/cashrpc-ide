import { Address } from './address';

import { bigIntToVmNumber, hexToBin, utf8ToBin } from '@bitauth/libauth';

export function castValueToBin(value: string | number | unknown): any {
  if (typeof value === 'number') {
    return bigIntToVmNumber(BigInt(value));
  } else if (typeof value === 'string' && (value as string).startsWith('<')) {
    return value;
  } else if (typeof value === 'string' && (value as string).startsWith('0x')) {
    return hexToBin(value.replace('0x', ''));
  } else if (typeof value === 'string') {
    if (
      value.startsWith('bitcoincash:') ||
      value.startsWith('bchtest') ||
      value.startsWith('bchreg')
    ) {
      return Address.fromCashAddrOrLegacy(value).toLockscriptBytes();
    }

    return utf8ToBin(value);
  }

  return value;
}

export function castCompileScriptPayload(params: any) {
  for (const [key, value] of Object.entries(params.data)) {
    params.data[key] = castValueToBin(value);
  }
}

export function castTransactionPayload(params: any) {
  for (const input of params.transaction.inputs || []) {
    input.outpointTransactionHash = castValueToBin(
      input.outpointTransactionHash
    );

    if (input.data) {
      for (const [key, value] of Object.entries(input.data)) {
        input.data[key] = castValueToBin(value);
      }
    }
  }

  for (const output of params.transaction.outputs || []) {
    if (output.data) {
      for (const [key, value] of Object.entries(output.data)) {
        output.data[key] = castValueToBin(value);
      }
    }

    if (output.token) {
      if (
        output.token.category.startsWith('<') &&
        output.token.category.endsWith('>')
      ) {
      } else {
        output.token.category = castValueToBin(output.token.category);
      }

      if (output.token.nft) {
        if (output.token.nft.commitment) {
          output.token.nft.commitment = castValueToBin(
            output.token.nft.commitment
          );
        }
      }

      if (typeof output.token.amount === 'number') {
        output.token.amount = BigInt(output.token.amount);
      }
    }

    if (typeof output.valueSatoshis === 'number') {
      output.valueSatoshis = BigInt(output.valueSatoshis);
    }
  }
}

export function castBatchPayload(payloads: any) {
  for (const tx of payloads) {
    castTransactionPayload(tx);
  }
}

export function castDataPayloads(payload: any) {
  if (Array.isArray(payload)) {
    castBatchPayload(payload);
  } else if (payload.transaction) {
    castTransactionPayload(payload);
  } else if (payload.data) {
    castCompileScriptPayload(payload);
  } else if (payload.message) {
    if (
      typeof payload.message === 'string' &&
      payload.message.startsWith('0x')
    ) {
      payload.message = hexToBin(payload.message);
    }
  }
}

export interface FileType {
  accept: Record<string, string[]>;
}

export interface FilePickerOptions {
  multiple?: boolean;
  types: FileType[];
}

export interface PickedFile {
  getFile: () => Promise<File>;
}

export function showOpenFilePicker(
  options: FilePickerOptions
): Promise<PickedFile[]> {
  return new Promise((resolve) => {
    const input = document.createElement('input');
    input.type = 'file';
    input.multiple = !!options.multiple;
    input.accept = options.types
      .map((type) =>
        Object.keys(type.accept)
          .flatMap((key) => type.accept[key])
          .join(',')
      )
      .join(',');

    input.addEventListener('change', () => {
      resolve(
        Array.from(input.files || []).map((file) => ({
          getFile: async () => file,
        }))
      );
    });

    input.click();
  });
}

export async function pickFiles(
  multiple = false
): Promise<Array<{ name: string; content: string }>> {
  const files = await showOpenFilePicker({
    multiple,
    types: [
      {
        accept: { 'application/json': ['*.json'] },
      },
    ],
  });

  const readFile = async (file: File): Promise<string> => {
    return new Promise<string>((resolve) => {
      const reader = new FileReader();
      reader.onload = () => {
        const fileContent = reader.result as string; // Type assertion for reader.result
        resolve(fileContent);
      };
      reader.readAsText(file);
    });
  };

  const readFiles = [];

  for (const file of files) {
    const fileHandle = await file.getFile();

    readFiles.push({
      name: fileHandle.name,
      content: await readFile(fileHandle),
    });
  }

  return readFiles;
}

export function parseExtendedJson(jsonString: string) {
  const uint8ArrayRegex = /^<Uint8Array: 0x(?<hex>[0-9a-f]*)>$/u;
  const bigIntRegex = /^<bigint: (?<bigint>[0-9]*)n>$/;

  return JSON.parse(jsonString, (_key, value) => {
    if (typeof value === 'string') {
      const bigintMatch = value.match(bigIntRegex);
      if (bigintMatch) {
        return BigInt(bigintMatch[1]);
      }
      const uint8ArrayMatch = value.match(uint8ArrayRegex);
      if (uint8ArrayMatch) {
        return hexToBin(uint8ArrayMatch[1]);
      }
    }
    return value;
  });
}
