import type { AuthenticationTemplate } from '@bitauth/libauth';
import { isHex } from '@bitauth/libauth';
import type { Artifact } from 'cashscript';

export interface GenerationOptions {
  /** Name of this template */
  name: string;
  /** Description for this template */
  description: string;
}

export class ArtifactsToAuthTemplate {
  constructor(
    /** Array of artifacts to include in this template */
    public readonly artifacts: Array<Artifact>,
    public readonly options: GenerationOptions
  ) {}

  public build() {
    // Define the base template.
    const template: AuthenticationTemplate = {
      $schema: 'https://ide.bitauth.com/authentication-template-v0.schema.json',
      name: this.options.name,
      description: this.options.description,
      entities: {
        common: {
          name: 'Common',
          variables: {},
        },
      },
      scripts: {},
      supported: ['BCH_2022_05'],
      version: 0,
    };

    // Iterate over each artifact and append it to the Libauth Template.
    this.artifacts.forEach((artifact) => {
      // Get the prefix for this contract.
      const contractPrefix = this.toSnakeCase(artifact.contractName);
      const bytecodeName = `${contractPrefix}.bytecode`;
      const lockscriptName = `${contractPrefix}.lock`;
      const lockscriptArgs: Array<string> = [];

      // Append the constructor arguments to our variables.
      artifact.constructorInputs.forEach((argument) => {
        // Ensure that an entity called "common" exists.
        // NOTE: Typescript quirk - should not have to do this.
        if (!template.entities['common'].variables) {
          throw new Error('Entity does not exist');
        }

        // Set the name of the variable.
        const variableName = this.toSnakeCase(argument.name);

        // Push the variable name onto our list of arguments.
        lockscriptArgs.push(`<${variableName}>`);

        // Add the argument to our list of variables.
        template.entities['common'].variables[variableName] = {
          name: this.toTitleCase(this.toSnakeCase(argument.name)),
          type: 'AddressData',
          description: this.inferTypeHint(argument.name, argument.type),
        };
      });

      // Add the bytecode to our scripts.
      template.scripts[bytecodeName] = {
        script: this.formatBytecode(artifact.bytecode).trim(),
      };

      // Add the lockscript to our scripts.
      template.scripts[lockscriptName] = {
        name: this.toTitleCase(lockscriptName),
        script: `${lockscriptArgs
          .reverse()
          .join('\n')}\n$(<${bytecodeName}>)`.trim(),
        lockingType: 'p2sh32',
      };

      // Append the ABI.
      artifact.abi.forEach((abi, index) => {
        // Snake-case the ABI (unlocking function) name.
        const abiName = this.toSnakeCase(abi.name);

        // Set the name of this unlock script.
        const unlockName = `${contractPrefix}.unlock.${abiName}`;

        // Array of arguments (push operations) needed to unlock.
        const unlockingArgs = [];

        // Append each argument.
        for (const argument of abi.inputs) {
          // Ensure that an entity called "common" exists.
          // NOTE: Typescript quirk - should not have to do this.
          if (!template.entities['common'].variables) {
            throw new Error('Entity does not exist');
          }

          // Set the name of the variable.
          const variableName = this.toSnakeCase(argument.name);

          // Push the variable name onto our list of arguments.
          unlockingArgs.push(`<${variableName}>`);

          // Add the argument to our list of variables.
          template.entities['common'].variables[variableName] = {
            name: this.toTitleCase(this.toSnakeCase(argument.name)),
            type: 'AddressData',
            description: this.inferTypeHint(argument.name, argument.type),
          };
        }

        // If there is only a single unlock, do not append an OP_X.
        const opCode = artifact.abi.length > 1 ? `OP_${index}` : '';

        // Add the unlock script
        template.scripts[unlockName] = {
          name: `${this.toTitleCase(contractPrefix)} - ${this.toTitleCase(
            abiName
          )}`,
          script: `${unlockingArgs.reverse().join('\n')}\n${opCode}`.trim(),
          unlocks: lockscriptName,
        };
      });
    });

    return template;
  }

  private formatBytecode(bytecode: string): string {
    return bytecode
      .split(' ')
      .map((op) => (isHex(op) ? `<0x${op}>` : op))
      .join('\n');
  }

  private toSnakeCase(str: string): string {
    // NOTE: Only works FROM camelCase.
    return str
      .split(/(?=[A-Z\d])/)
      .map((part) => part.toLowerCase())
      .join('_');
  }

  private toTitleCase(str: string): string {
    // NOTE: Only works FROM snake_case.
    return str
      .split('_')
      .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
      .join(' ');
  }

  private inferTypeHint(argName: string, argType: string): string | undefined {
    if (
      argName.endsWith('LockingBytecode') ||
      argName.endsWith('Lockscript') ||
      argName.endsWith('Address')
    ) {
      return 'lockscript';
    }

    if (argName.endsWith('PublicKey') || argName.endsWith('PubKey')) {
      return 'publicKey';
    }

    if (argName.endsWith('Timestamp')) {
      return 'unixTimestamp';
    }

    if (argType === 'int') {
      return 'number';
    }
  }
}
