## Usage Notes

1. There's a help button near the top right on most pages of the tool that provides additional info for each component.
2. The Project format is not final, but I will make old versions of this tool available so that you can migrate to new formats.
    1. If possible, I'll try to do this automatically upon import (but please do not depend upon me doing this).
3. This is what I consider a pre-alpha release. There will probably be many bugs still (and breaking changes incoming).
4. I've included two examples:
    1. Sahid's Flipstarter (this contract is really nice and simple to follow and demonstrates how Token Genesis can be done)
    2. Early BCH Guru Contracts (these are earlier versions I was sent - but they demonstrate some advanced capabilities that LibAuth Templates give us)
5. Transaction Validation is built into CashRPC.
    1. If a Transaction Fails to build, you will get an error message and you can view the full stack trace for each input in your Cashonize Wallet's Browser Console.
6. This tool is currently restricted Chipnet.

## Brief Overview

1. To help preserve privacy (inc. with HD Wallets) and improve security, the Session established between DAPP (Service) and WALLET uses a Sandboxed Keypair generated using `sandboxedPrivateKey = sha256(${masterPrivateKey}cashrpc-over-wc:${domainName})`, where:
    1. For non-HD Wallets, the `masterPrivateKey` is just the key of the Wallet.
    2. For HD Wallets, the `masterPrivateKey` can be a derivation path of the Wallet's seed.
2. This uses LibAuth templates. In future, these template can be hashed and whitelisted (trusted) by a wallet.
3. For security, the service can only specify inputs and outputs by their corresponding scripts in the provided template (no raw locking/unlocking bytecode allowed). In this sense, there are currently four different kinds of Inputs and Outputs:
    1. Template-scoped Inputs (uses a `script` from template)
    2. Template-scoped Outputs (uses a `script` from template)
    3. Wallet-scoped Inputs (must contain a Whitelisted Token Category - see below)
    4. Wallet-scoped Outputs (A Change Ooutput - Lockscript will be provided by Wallet)
4. CashRPC will provide additional UTXOs to automatically meet the Satoshi and Fungible Token amounts that are summed in the outputs. In this respect, UTXO selection is done by the Wallet, but the DAPP can also provide its own inputs (and whitelisted NFTs from the wallet: see next point).
5. The DAPP CAN request a WALLET's UTXOs provided they a) contain tokens and b) access to those token category ID's have been granted upon Session Negotiation (`allowedTokens` as an array of category ids or `*` for ALL tokens). This allows us to support token-specific use-cases (e.g. a particular NFT as input or more vague Crypto-Exchange use-cases).
6. To prevent standards fracturing, the underlying RPC's are intended to be transport agnostic: They should be applicable to HTTP, LibP2P any other transports (not just WalletConnect). Thus, we should be able to use them for a future HTTP-based Payment Protocols, etc.
7. Hopefully, in future when we have stateful wallets, we will be to store the AddressData of the transactions and converge with what BitJSON is building. The idea is that a wallet CAN (if the template is natively supported) save the data payloads and use the built-in Wallet UX to execute contract actions. But, for WC, the idea is that the DAPP (Service) stores the data associated with a given WC account.
8. Type-hints (for Wallet Display of Data Variables) are currently stored as plaintext in the "description" field of a variable in the template. This is a temporary workaround.
    1. We want to think a little about how to handle this as they are not always static (e.g. using oracles.cash, we may set a price - but we want that to show depending upon which Oracle was chosen in another variable according to the scaling factor). Therefore, we ideally want some "intelligence" to these typehints.
10. The Sandboxed Accounts are currently identified by Public Key (not address) in the form `bch:${chain}:${publicKey}`.
    1. This does break from WC convention but simplifies implementation.
    2. It also discourages DAPPs from sending to funds to these SANDBOXED accounts.
    3. It might give us interesting capabilities wrt signing/encryption (ECIES) within a service.
11. There are a few limitations currently with this approach:
    1. Multi-party contracts are either not possible or very clunky to implement. There's a constraint currently that the Wallet MUST be able to sign a transaction successfully. I haven't worked out a better approach for this yet, but maybe someone might have suggestions for a more versatile approach.

## Implementation

1. All code is available under the top-right Git Icon.
2. There are currently three different repositories (but there will be four in future).
    1. cashconnect (The CashRPC over WC Library)
    2. cashonize (The fork of Cashonize that implements CashRPC over WC)
    3. CashRPC IDE (the tool built using Quasar)
    4. (TODO) CashRPC (In future, this will be split out from the Wallet Connect implementation for use in Payment Protocols and even internal Wallet Use)
3. Please do not submit MRs quite yet. There is a lot of refactoring I still need to do and a lot of mess to cleanup.
4. I'm putting a lot of effort into trying to make CashRPC over WC very easy for Web Wallets to implement via the library.
    1. Excluding UI, it only takes around 150 LoC - see `wallet-connect-2.js` in Cashonize repository and Ctrl+F `WalletConnectService`.
    2. DO NOT try to implement in your Wallet yet. There's a number of changes I need to make still (see TODOs below).
5. For Dapp Developers, there will be a `WalletConnectDapp` class that tries to handle some of the quirkier aspects of WalletConnect.
    1. You are NOT bound to this - but it should ease implementation and (hopefully) handle some of the difficult edge-cases.

## TODOs (Big Breaking Changes)

1. Tests will eventually live in the "template" under "actions". We want to do this for security:
    1. Some contract flows expect a Payout Address. For security, we do not want the script for this to be callable directly, but only with a corresponding Unlock (for example).
2. I would like to find a way to stash metadata about which Inputs belong to the Wallet in the response payloads (to ease Wallet UI Implementations).
3. Separate CashRPC into its own Library (and add validations).
4. Many refactors/cleanups in the code (it is a mess currently).
5. Casting of values in the tool is very clunky. I'll probably rewrite to use JS (as opposed to JSON) and make casting explicit.
6. Not a breaking change, but the Cashonize UI for this needs A LOT of work.

### "Actions" (WRT TODOs Point 1)

The format for these will be similar to Tests. Format will probably look something like:

```
"actions: {
  "create_campaign": {
    "name": "Some Action",
    "description": "...",
    "signerKey": "user",
    "transactions": [
      // ...
    ]
  }
}
```
