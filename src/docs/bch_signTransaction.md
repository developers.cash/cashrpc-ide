## bch_signTransaction_V0

**THE TRANSACTION SHAPES HERE WILL BE REFACTORED AND MOVED UNDER THE TEMPLATE IN FUTURE**

In practice, all data payloads MUST be binary encoded. However, this tool will attempt to cast them for you.

```
// Precede all hexadecimal values that should be cast to binary types with 0x. Example:
"outpointTransactionHash": "0x17867afe9183aac784b2060490ad0db742dc490d265600228ea554b4bc9e5996"

// If a string is prefixed with "bitcoincash:", "bchtest:" or "bchreg:" it will automatically be cast to Locking Bytecode. Example:
"payout_bytecode": "bitcoincash:qrs6w2f5026m78z90l9lpsp4e6r6sxfh0yhvgcfdru"

// You can access the result of previous transaction by using a string placeholder as follows:
// NOTE: Will insert the transactionHash generated from the first transaction.
"outpointTransactionHash": "<0.transactionHash>",

// The above also applies to the "token.category" field:
"category": "<0.transactionHash>",
```

I will document this better once I've refactored to place this under templates.

Anticipated structure will likely be something like:

```
"actions: {
  "create_campaign": {
    "name": "Some Action",
    "description": "...",
    "signerKey": "user",
    "transactions": [
      // ...
    ]
  }
}
```
