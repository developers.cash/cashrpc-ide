## Signer

These are the Session Proposal Parameters that will be sent to Wallet Connect.

### allowedTokens

To improve privacy (particularly with HD Wallets), we want to lock down which UTXOs the service can retrieve. Right now, this proposal doesn't have a `bch_getUnspents` RPC call - instead, it provides a `bch_getTokens` RPC call which will provide all unspents that contain the token categories that are listed in this field. There are some use-cases (e.g. imagine a "DEX" or "exchange" or whatever) that should be able to access all tokens. In this case, an asterisk can be used.

**Would like feedback on this - unsure whether it is sufficient to meet all use-cases.**

### template

The template is also required at this point (assumption: better for service to provide the template to the wallet ONCE and have the user approve as opposed to each RPC call?), but will be automatically injected by this tool.

**Would like people's thoughts on that assumption.**
