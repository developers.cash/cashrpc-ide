## Generate Template

The template on the left will be **merged** into the generated template (which will appear on the right).

It is recommended that you first generate the template and then copy > paste > customize parameters for use with a Wallet (the "Signer").

Though there is no convention enforced, you may wish to indicate that a particular script is intended for use with a Walley by suffixing it with `.wallet` (or similar).

### CashASM

```
# To push a script or variable, wrap in:
<YOUR_SCRIPT_OR_VARIABLE>

# To evaluate, wrap in:
# NOTE: Evaluations will OMIT the OP_PUSH.
$(<YOUR_SCRIPT_OR_VARIABLE>)
```

Note that the result of inline/anonymous evaluations will not be returned. You should instead create named scripts for these and then reference them in your `.wallet` lockscript.

For example:

```
"resolved.USER_pubkey": {
  "name": "Resolved User Public Key",
  "script": "$(<USER.public_key>)"
},
```

Note: The `resolved.` prefix is not required. You may name these however you like.

### Helpful Snippets

**Public Key/Public Key Hash**

```
// Get the public key of the Signer.
"resolved.USER_pubkey": {
  "name": "Resolved User Public Key",
  "script": "$(<USER.public_key>)"
},

// Get the public key hash of the Signer.
"resolved.USER_pubkeyHash": {
  "name": "Resolved User Public Key Hash",
  "script": "$(<player_1_key.public_key> OP_HASH160)"
},
```

**Datasig**

```
// This script can be used to construct a Schnorr datasig using the given signer.
// NOTE: Replace "SIGNER" with the pertinent key and replace "SOME_VARIABLE" with the variable you wish to sign.
"resolved.some_datasig": {
  "name": "Some Datasig",
  "script": "$(<SIGNER.schnorr_data_signature.SOME_VARIABLE>)"
},
```

**Lockscript Construction (P2SH32)**

```
// This script can be used in situations where you need to construct a Lockscript as input to another contract.
// NOTE: Replace "SOME_CONTRACT" with the name of the contract.
"resolved.locking_example": {
  "name": "Example Locking Example",
  "script": "OP_HASH256 <$(<SOME_CONTRACT.lock> OP_HASH256)> OP_EQUAL"
},
```

**Sandbox P2PKH**

```
// This script is used to lock a P2PKH to the sandboxed Keypair. It can be used for Token Gensis, etc.
// NOTE: Replace "SIGNER" with the pertinent key.
"sandbox_p2pkh.lock.wallet": {
  "lockingType": "standard",
  "name": "P2PKH Lock",
  "script": "OP_DUP\nOP_HASH160 <$(<SIGNER.public_key> OP_HASH160\n)> OP_EQUALVERIFY\nOP_CHECKSIG"
},
// This script unlocks the output above (so that we can use it to mint tokens).
"sandbox_p2pkh.unlock.wallet": {
  "name": "Unlock",
  "script": "<SIGNER.schnorr_signature.all_outputs>\n<SIGNER.public_key>",
  "unlocks": "sandbox_p2pkh.lock.wallet"
},
```
