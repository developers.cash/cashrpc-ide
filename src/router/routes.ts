import { RouteRecordRaw } from 'vue-router';

// Pages and Layouts.
// NOTE: Usually we would dynamically import pages using the `() => import()` form.
//       However, this can cause caching issues for users where the a user clicks a page that points to a hash that no longer exists.
//       So we bundle into a single JS File by importing directly.
import MainLayout from '../layouts/MainLayout.vue';
import ProjectPage from '../pages/ProjectPage.vue';
import ContractPage from '../pages/ContractPage.vue';
import GeneratePage from '../pages/GeneratePage.vue';
import WalletConnectPage from '../pages/WalletConnectPage.vue';
import TestPage from '../pages/TestPage.vue';
import ErrorNotFound from '../pages/_ErrorNotFound.vue';

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    component: MainLayout,
    children: [
      // Main App
      {
        path: '',
        component: ProjectPage,
      },
      {
        path: 'contract/:i',
        component: ContractPage,
      },
      {
        path: 'generate',
        component: GeneratePage,
      },
      {
        path: 'wallet-connect',
        component: WalletConnectPage,
      },
      {
        path: 'test/:i',
        component: TestPage,
      },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: ErrorNotFound,
  },
];

export default routes;
